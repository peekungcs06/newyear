<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RandomAwardController extends CI_Controller {


    public function index()
    {
        $this->load->view('template/header');
        $this->load->view('award/RandomAward');
        $this->load->view('template/footer');
    }
}
